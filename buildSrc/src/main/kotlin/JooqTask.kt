import org.flywaydb.core.Flyway
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.InputDirectory
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.SourceSetContainer
import org.gradle.api.tasks.TaskAction
import org.gradle.kotlin.dsl.getByType
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.*
import org.jooq.meta.jaxb.Target
import java.io.File
import java.nio.file.Paths

open class JooqTask : DefaultTask() {

    @get:OutputDirectory
    val outputPath = File(project.buildDir, "jooq")

    private val resourceDir = project.extensions
        .getByType<SourceSetContainer>()
        .getByName("main")
        .resources
        .srcDirs
        .firstOrNull() ?: error("Resources not found")

    @get:InputDirectory
    val migrationDir: File = Paths.get(resourceDir.absolutePath, "db", "migration").toFile()

    @TaskAction
    fun generateJooq() {
        outputPath.deleteRecursively()

        val tempDir = System.getProperty("java.io.tmpdir").replace('\\', '/')
        val jdbcUrl = "jdbc:h2:$tempDir/simplenotes"

        Flyway.configure()
            .locations("filesystem:$migrationDir")
            .dataSource(jdbcUrl, "", "")
            .load()
            .migrate()

        val conf = Configuration()
            .withJdbc(Jdbc().withUrl(jdbcUrl).withUsername("").withPassword(""))
            .withGenerator(
                Generator()
                    .withDatabase(
                        Database().withName("org.jooq.meta.h2.H2Database").withInputSchema("PUBLIC")
                            .withExcludes("flyway_schema_history")
                    )
                    .withGenerate(Generate()).withName("org.jooq.codegen.KotlinGenerator")
                    .withTarget(
                        Target().withPackageName("be.simplenotes.persistence.jooq")
                            .withDirectory(outputPath.absolutePath)
                    )
            )

        GenerationTool.generate(conf)
    }
}