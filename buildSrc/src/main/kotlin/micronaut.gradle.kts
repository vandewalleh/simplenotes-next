plugins {
    kotlin("jvm") apply false
    kotlin("kapt")
}

dependencies {
    kapt("io.micronaut:micronaut-inject-java:2.3.3")
    implementation("io.micronaut:micronaut-inject:2.3.3")
    implementation("io.micronaut.kotlin:micronaut-kotlin-extension-functions:2.3.0") {
        exclude(group = "com.fasterxml.jackson.module")
    }
}

kapt {
    arguments {
        arg("micronaut.processing.incremental", true)
    }
}
