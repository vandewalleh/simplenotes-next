plugins {
    `kotlin-dsl`
}

kotlinDslPluginOptions {
    experimentalWarning.set(false)
}

repositories {
    gradlePluginPortal()
}

dependencies {
    implementation(platform("org.jetbrains.kotlin:kotlin-bom:1.4.31"))
    implementation("org.jetbrains.kotlin:kotlin-gradle-plugin:1.4.31")
    implementation("org.jetbrains.kotlin:kotlin-serialization:1.4.31")
    implementation("com.h2database:h2:1.4.200")
    implementation("org.flywaydb:flyway-core:7.5.4")
    implementation("org.jooq:jooq:3.14.4")
    implementation("org.jooq:jooq-meta:3.14.4")
    implementation("org.jooq:jooq-codegen:3.14.4")
}
