plugins {
    id("kotlin-application")
    id("micronaut")
    kotlin("plugin.serialization")
}

dependencies {
    implementation("org.slf4j:slf4j-api:2.0.0-alpha1")
    runtimeOnly("ch.qos.logback:logback-classic:1.3.0-alpha5")

    implementation(platform("org.http4k:http4k-bom:4.4.0.1"))
    implementation("org.http4k:http4k-core")
    implementation("org.http4k:http4k-server-jetty")

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json-jvm:1.1.0")

    implementation("com.h2database:h2:1.4.200")
    implementation("org.flywaydb:flyway-core:7.5.4")
    implementation("org.jooq:jooq:3.14.4")
    implementation("org.simpleflatmapper:sfm-jooq:8.2.3")

    implementation("com.amdelamar:jhash:2.2.0")
    implementation("io.jsonwebtoken:jjwt-api:0.11.2")
    runtimeOnly("io.jsonwebtoken:jjwt-impl:0.11.2")

    implementation("io.arrow-kt:arrow-core-data:0.11.0")
    implementation("org.ocpsoft.prettytime:prettytime:5.0.0.Final")
}

application {
    mainClass.set("be.simplenotes.SimplenotesKt")
}

tasks.register<JooqTask>("generateJooq")
kotlin.sourceSets["main"].kotlin.srcDir(File(project.buildDir, "jooq"))
