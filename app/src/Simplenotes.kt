package be.simplenotes

import be.simplenotes.controllers.Controller
import be.simplenotes.filters.ErrorFilter
import be.simplenotes.persistence.Migrations
import io.micronaut.context.ApplicationContext
import io.micronaut.context.annotation.Property
import io.micronaut.context.getBean
import org.http4k.core.RequestContexts
import org.http4k.core.then
import org.http4k.filter.CorsPolicy.Companion.UnsafeGlobalPermissive
import org.http4k.filter.ServerFilters.Cors
import org.http4k.filter.ServerFilters.InitialiseRequestContext
import org.http4k.routing.routes
import org.http4k.server.Jetty
import org.http4k.server.asServer
import org.slf4j.LoggerFactory
import java.lang.management.ManagementFactory
import javax.inject.Singleton

fun main() {
    val appCtx = ApplicationContext.build()
        .deduceEnvironment(false)
        .start()

    appCtx.getBean<Migrations>().migrate()
    appCtx.getBean<Simplenotes>().start()
}

@Singleton
class Simplenotes(
    @Property(name = "server.port") private val port: Int,
    private val errorFilter: ErrorFilter,
    private val requestContexts: RequestContexts,
    private val controllers: List<Controller>
) {
    fun start() {
        val router = routes(*controllers.map { it.routes() }.toTypedArray())

        Cors(UnsafeGlobalPermissive)
            .then(errorFilter)
            .then(InitialiseRequestContext(requestContexts))
            .then(router)
            .asServer(Jetty(port))
            .start()

        val now = System.currentTimeMillis()
        val start = ManagementFactory.getRuntimeMXBean().startTime
        LoggerFactory.getLogger(javaClass).info("Server started in {}ms", now - start)
    }
}
