package be.simplenotes.services

import arrow.core.Either
import arrow.core.left
import arrow.core.right
import be.simplenotes.persistence.User
import be.simplenotes.persistence.UserRepository
import com.amdelamar.jhash.Hash
import com.amdelamar.jhash.algorithms.Type
import javax.inject.Singleton

sealed class RegisterError {
    object UserExists : RegisterError()
    // TODO: validation
}

sealed class LoginError {
    object Unregistered : LoginError()
    object WrongPassword : LoginError()
    // TODO: validation
}

@Singleton
class PasswordHasher {
    fun hashPw(password: String) = Hash.password(password.toCharArray()).algorithm(Type.BCRYPT).create()!!
    fun checkPw(hash: String, password: String) =
        Hash.password(password.toCharArray()).algorithm(Type.BCRYPT).verify(hash)
}

@Singleton
class UserService(
    private val userRepository: UserRepository,
    private val jwt: SimpleJwt,
    private val passwordHasher: PasswordHasher
) {
    // TODO: validation
    fun register(user: User): RegisterError? =
        if (userRepository.exists(user.username)) RegisterError.UserExists
        else if (!userRepository.create(user.copy(password = passwordHasher.hashPw(user.password)))) RegisterError.UserExists
        else null

    // TODO: validation
    fun login(user: User): Either<LoginError, JwtToken> {
        val userDb = userRepository.find(user.username) ?: return LoginError.Unregistered.left()
        if (!passwordHasher.checkPw(userDb.password, user.password)) return LoginError.WrongPassword.left()
        return jwt.sign(UserPrincipal(userDb.id, userDb.username)).right()
    }

}