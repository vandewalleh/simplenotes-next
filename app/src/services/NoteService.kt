package be.simplenotes.services

import be.simplenotes.persistence.Note
import be.simplenotes.persistence.NoteRepository
import kotlinx.serialization.Serializable
import org.ocpsoft.prettytime.PrettyTime
import javax.inject.Singleton

@Serializable
data class NoteWithLastModifiedAndPrettyDate(
    val title: String,
    val content: String,
    val tags: List<String>,
    val lastModified: String
)

@Singleton
class NoteService(private val noteRepository: NoteRepository) {

    private val prettyTime = PrettyTime()

    fun createNote(userId: Long, note: Note) {
        // TODO: validation etc
        noteRepository.create(userId, note)
    }

    fun findAll(userId: Long) = noteRepository.findAll(userId).map {
        NoteWithLastModifiedAndPrettyDate(
            it.title,
            it.content,
            it.tags,
            prettyTime.format(it.lastModified)
        )
    }

}
