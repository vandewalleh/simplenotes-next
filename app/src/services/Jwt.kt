package be.simplenotes.services

import io.jsonwebtoken.JwtException
import io.jsonwebtoken.Jwts
import io.jsonwebtoken.io.Deserializer
import io.jsonwebtoken.io.Serializer
import io.jsonwebtoken.security.Keys
import io.micronaut.context.annotation.Property
import kotlinx.serialization.json.*
import java.time.Instant
import java.time.temporal.ChronoUnit
import java.util.*
import javax.inject.Singleton

data class UserPrincipal(val id: Long, val username: String)

inline class JwtToken(val value: String)

@Singleton
class SimpleJwt(
    @Property(name = "jwt.key") key: String,
    @Property(name = "jwt.validity") validity: Long,
    @Property(name = "jwt.unit") unit: ChronoUnit,
    private val serializer: JwtSerde
) {
    private val validity = unit.duration.multipliedBy(validity)
    private val key = Keys.hmacShaKeyFor(key.encodeToByteArray())

    private fun getFromClaims(it: Map<String, Any?>): UserPrincipal =
        UserPrincipal(it["id"]!!.toString().toLong(), it["username"]!!.toString())

    fun sign(principal: UserPrincipal): JwtToken {
        val map = mapOf("id" to principal.id, "username" to principal.username)
        val now = Instant.now()
        return Jwts.builder()
            .setClaims(map)
            .setIssuedAt(Date.from(now))
            .setExpiration(Date.from(now + validity))
            .signWith(key)
            .serializeToJsonWith(serializer)
            .setIssuer("Simplenotes")
            .compact()
            .let(::JwtToken)
    }

    fun verify(token: String): UserPrincipal? = try {
        Jwts.parserBuilder()
            .deserializeJsonWith(serializer)
            .requireIssuer("Simplenotes")
            .setSigningKey(key)
            .build()
            .parseClaimsJws(token)
            .body
            .let(::getFromClaims)
    } catch (e: JwtException) {
        null
    } catch (e: IllegalArgumentException) {
        null
    }

}

interface JwtSerde : Serializer<Map<String, Any?>>, Deserializer<Map<String, Any?>>

@Singleton
class KotlinxSerializer : JwtSerde {
    override fun serialize(content: Map<String, Any?>): ByteArray {
        val jsonObject = buildJsonObject {
            for ((key, value) in content) {
                when (value) {
                    is Boolean? -> put(key, value)
                    is Number? -> put(key, value)
                    is String? -> put(key, value)
                    else -> throw UnsupportedOperationException("Can only serialize primitive types")
                }
            }
        }
        return Json.encodeToString(JsonObject.serializer(), jsonObject).encodeToByteArray()
    }

    override fun deserialize(bytes: ByteArray): Map<String, Any?> {
        val content = bytes.decodeToString()
        val jsonObject = Json.decodeFromString(JsonObject.serializer(), content)
        val map = hashMapOf<String, String?>()
        for ((key, value) in jsonObject) {
            if (value is JsonPrimitive) map[key] = value.contentOrNull
            else throw UnsupportedOperationException()
        }
        return map
    }
}
