package be.simplenotes.persistence

import be.simplenotes.persistence.jooq.tables.references.USERS
import kotlinx.serialization.Serializable
import org.jooq.DSLContext
import org.jooq.impl.DSL.asterisk
import javax.inject.Singleton

@Serializable
data class PersistedUser(val id: Long, val username: String, val password: String) {
    override fun toString() = "PersistedUser(id=$id, username='$username')"
}

@Serializable
data class User(val username: String, val password: String) {
    override fun toString() = "User(username='$username')"
}

@Singleton
class UserRepository(private val db: DSLContext) {

    fun create(user: User) = tryInsert(
        db.insertInto(USERS, USERS.USERNAME, USERS.PASSWORD)
            .values(user.username, user.password)
    )

    fun find(username: String) = db.select(asterisk())
        .from(USERS)
        .where(USERS.USERNAME.eq(username))
        .fetchOneInto(PersistedUser::class.java)

    fun exists(username: String) = db.fetchExists(USERS, USERS.USERNAME.eq(username))

}