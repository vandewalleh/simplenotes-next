package be.simplenotes.persistence

import io.micronaut.context.annotation.Factory
import io.micronaut.context.annotation.Property
import org.flywaydb.core.Flyway
import org.h2.jdbcx.JdbcDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.conf.RenderNameCase
import org.jooq.conf.RenderQuotedNames
import org.jooq.conf.Settings
import org.jooq.impl.DSL
import org.jooq.impl.DefaultConfiguration
import org.simpleflatmapper.jooq.JooqMapperFactory
import javax.inject.Singleton
import javax.sql.DataSource

@Factory
class PersistenceModule {

    @Singleton
    fun dataSource(@Property(name = "datasource.url") url: String) = JdbcDataSource().apply { setURL(url) }

    @Singleton
    fun flyway(dataSource: DataSource) = Flyway.configure().dataSource(dataSource).load()!!

    @Singleton
    fun jooq(dataSource: DataSource): DSLContext {
        val settings = Settings().apply {
            isRenderSchema = false
            renderQuotedNames = RenderQuotedNames.NEVER
            renderNameCase = RenderNameCase.AS_IS
            isCacheRecordMappers = true
        }

        val config = DefaultConfiguration().apply {
            setSettings(settings)
            setDataSource(dataSource)
            setSQLDialect(SQLDialect.H2)
            set(JooqMapperFactory.newInstance().ignorePropertyNotFound().newRecordMapperProvider())
        }

        return DSL.using(config)
    }

}