package be.simplenotes.persistence

import be.simplenotes.persistence.jooq.tables.references.NOTES
import be.simplenotes.persistence.jooq.tables.references.TAGS
import kotlinx.serialization.Serializable
import org.jooq.DSLContext
import org.jooq.Query
import org.jooq.TransactionalRunnable
import org.jooq.impl.DSL.*
import java.time.Instant
import java.util.*
import javax.inject.Singleton
import kotlin.collections.ArrayList

@Serializable
data class Note(val title: String, val content: String, val tags: List<String>)

data class NoteWithLastModified(
    val title: String,
    val content: String,
    val tags: List<String>,
    val lastModified: Instant
)

@Singleton
class NoteRepository(private val db: DSLContext) {

    fun create(userId: Long, note: Note) = db.transaction(TransactionalRunnable {
        val uuid = UUID.randomUUID()
        val batches = ArrayList<Query>(note.tags.size + 1)

        batches += db.insertInto(NOTES, NOTES.ID, NOTES.USER_ID, NOTES.TITLE, NOTES.CONTENT)
            .values(uuid, userId, note.title, note.content)

        for (tag in note.tags) {
            batches += db.insertInto(TAGS, TAGS.NOTE_ID, TAGS.NAME).values(uuid, tag)
        }

        db.batch(batches).execute()
    })

    fun findAll(userId: Long): List<NoteWithLastModified> = db.select(
        NOTES.TITLE,
        NOTES.CONTENT,
        coalesce(arrayAgg(TAGS.NAME), inline("[]")).`as`("tags"),
        coalesce(NOTES.UPDATED, NOTES.CREATED).`as`("lastModified")
    )
        .from(NOTES)
        .leftOuterJoin(TAGS).on(TAGS.NOTE_ID.eq(NOTES.ID))
        .where(NOTES.USER_ID.eq(userId), NOTES.TRASH.eq(inline(false)))
        .groupBy(NOTES.ID)
        .fetchInto(NoteWithLastModified::class.java)
}

