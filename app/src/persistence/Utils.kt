package be.simplenotes.persistence

import org.jooq.Query
import org.jooq.exception.DataAccessException
import org.jooq.exception.SQLStateClass

fun tryInsert(query: Query): Boolean = try {
    query.execute() == 1
} catch (e: DataAccessException) {
    if (e.sqlStateClass() == SQLStateClass.C23_INTEGRITY_CONSTRAINT_VIOLATION) false
    else throw e
}