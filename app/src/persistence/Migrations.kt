package be.simplenotes.persistence

import org.flywaydb.core.Flyway
import javax.inject.Singleton

@Singleton
class Migrations(private val flyway: Flyway) {
    fun migrate() {
        flyway.migrate()
    }

    fun clean() {
        flyway.clean()
    }
}