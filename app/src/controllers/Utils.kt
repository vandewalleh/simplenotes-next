package be.simplenotes.controllers

import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status

@Suppress("NOTHING_TO_INLINE")
inline operator fun Status.invoke() = Response(this)

class JsonLens<T>(private val serializer: KSerializer<T>) {
    operator fun invoke(value: T, response: Response) =
        response
            .header("Content-Type", "application/json")
            .body(Json.encodeToString(serializer, value))

    operator fun invoke(request: Request): T = Json.decodeFromString(serializer, request.bodyString())
}
