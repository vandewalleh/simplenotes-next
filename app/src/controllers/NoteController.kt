package be.simplenotes.controllers

import be.simplenotes.filters.AuthContextKey
import be.simplenotes.filters.AuthFilter
import be.simplenotes.persistence.Note
import be.simplenotes.services.NoteService
import be.simplenotes.services.NoteWithLastModifiedAndPrettyDate
import kotlinx.serialization.builtins.ListSerializer
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status.Companion.OK
import org.http4k.core.then
import org.http4k.routing.bind
import org.http4k.routing.routes
import javax.inject.Singleton

@Singleton
class NoteController(
    private val noteService: NoteService,
    private val authContextKey: AuthContextKey,
    private val authFilter: AuthFilter,
) : Controller {
    override fun routes() = authFilter.then(
        routes(
            "/" bind Method.POST to ::createNote,
            "/" bind Method.GET to ::getNotes
        ).withBasePath("/notes")
    )

    private val noteLens = JsonLens(Note.serializer())
    private val notesLens = JsonLens(ListSerializer(NoteWithLastModifiedAndPrettyDate.serializer()))

    private fun createNote(request: Request): Response {
        val userId = authContextKey[request].id
        noteService.createNote(userId, noteLens(request))
        return OK()
    }

    private fun getNotes(request: Request): Response {
        val userId = authContextKey[request].id
        val notes = noteService.findAll(userId)
        return notesLens(notes, OK())
    }

}
