package be.simplenotes.controllers

import org.http4k.routing.RoutingHttpHandler

interface Controller {
    fun routes(): RoutingHttpHandler
}