package be.simplenotes.controllers

import be.simplenotes.filters.AuthContextKey
import be.simplenotes.filters.AuthFilter
import be.simplenotes.persistence.User
import be.simplenotes.services.LoginError
import be.simplenotes.services.RegisterError
import be.simplenotes.services.UserService
import kotlinx.serialization.Serializable
import org.http4k.core.Method.GET
import org.http4k.core.Method.POST
import org.http4k.core.Request
import org.http4k.core.Status.Companion.CONFLICT
import org.http4k.core.Status.Companion.NOT_FOUND
import org.http4k.core.Status.Companion.OK
import org.http4k.core.Status.Companion.UNAUTHORIZED
import org.http4k.core.then
import org.http4k.routing.bind
import org.http4k.routing.routes
import javax.inject.Singleton

@Singleton
class UserController(
    private val userService: UserService,
    private val authContextKey: AuthContextKey,
    private val authFilter: AuthFilter,
) : Controller {

    override fun routes() = routes(
        "/register" bind POST to ::register,
        "/login" bind POST to ::login,
        authFilter.then("/protected" bind GET to ::protected)
    )

    private val userLens = JsonLens(User.serializer())
    private val messageLens = JsonLens(Message.serializer())
    private val loginResponseLens = JsonLens(LoginResponse.serializer())

    private fun register(request: Request) = when (userService.register(userLens(request))) {
        RegisterError.UserExists -> messageLens(Message("User already exists"), CONFLICT())
        null -> messageLens(Message("User created"), OK())
    }

    private fun login(request: Request) = userService.login(userLens(request)).fold(
        { error ->
            when (error) {
                LoginError.Unregistered -> messageLens(Message("User not found"), NOT_FOUND())
                LoginError.WrongPassword -> messageLens(Message("Wrong password"), UNAUTHORIZED())
            }
        },
        { token -> loginResponseLens(LoginResponse(token.value), OK()) }
    )

    private fun protected(request: Request) = OK().body("Welcome ${authContextKey[request].username}")

}

@Serializable
data class Message(val message: String)

@Serializable
data class LoginResponse(val token: String)
