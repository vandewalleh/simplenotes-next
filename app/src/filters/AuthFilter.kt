package be.simplenotes.filters

import be.simplenotes.controllers.invoke
import be.simplenotes.services.SimpleJwt
import be.simplenotes.services.UserPrincipal
import io.micronaut.context.annotation.Factory
import org.http4k.core.*
import org.http4k.core.Status.Companion.UNAUTHORIZED
import org.http4k.lens.BiDiLens
import org.http4k.lens.RequestContextKey
import javax.inject.Singleton

@Singleton
class AuthFilter(private val simpleJwt: SimpleJwt, private val authContextKey: AuthContextKey) : Filter {
    override fun invoke(next: HttpHandler): HttpHandler = { request ->
        request.header("Authorization")
            ?.removePrefix("Bearer ")
            ?.let { token -> simpleJwt.verify(token) }
            ?.let { principal -> next(request.with(authContextKey of principal)) }
            ?: UNAUTHORIZED()
    }
}

typealias AuthContextKey = BiDiLens<Request, UserPrincipal>

@Factory
class RequestContextFactory {

    @Singleton
    fun requestContexts() = RequestContexts()

    @Singleton
    fun authContextKey(requestContexts: RequestContexts) =
        RequestContextKey.required<UserPrincipal>(requestContexts, name = "auth")

}