package be.simplenotes.filters

import be.simplenotes.controllers.invoke
import org.http4k.core.Filter
import org.http4k.core.HttpHandler
import org.http4k.core.Status.Companion.INTERNAL_SERVER_ERROR
import org.slf4j.LoggerFactory
import javax.inject.Singleton

@Singleton
class ErrorFilter : Filter {
    private val logger = LoggerFactory.getLogger(javaClass)

    override fun invoke(next: HttpHandler): HttpHandler = { request ->
        try {
            next(request)
        } catch (t: Throwable) {
            logger.error("", t)
            INTERNAL_SERVER_ERROR()
        }
    }
}