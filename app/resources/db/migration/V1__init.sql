create table Users
(
    id       long auto_increment,
    username varchar(50)  not null,
    password varchar(255) not null,

    constraint PK_USERS primary key (id),
    constraint AK_USERNAME unique (username)
);

create table Notes
(
    id      uuid        not null default RANDOM_UUID(),
    user_id long        not null,
    title   varchar(50) not null,
    content clob        not null,
    trash   boolean     not null default false,
    created timestamp   not null default CURRENT_TIMESTAMP(),
    updated timestamp,

    constraint PK_NOTES primary key (id),
    constraint FK_NOTES_USERS foreign key (user_id) references Users (id) on delete cascade
);

create table Tags
(
    note_id uuid        not null,
    name    varchar(30) not null,

    constraint PK_TAGS primary key (note_id, name),
    constraint FK_TAGS_NOTES foreign key (note_id) references Notes (id) on delete cascade
);
