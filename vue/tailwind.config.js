const colors = require('tailwindcss/colors')

module.exports = {
    purge: ['./index.html', './src/**/*.{vue,ts}'],
    darkMode: false, // or 'media' or 'class'
    theme: {
        colors: {
            transparent: 'transparent',
            current: 'currentColor',
            black: colors.black,
            white: colors.white,
            gray: colors.coolGray,
            red: colors.red,
            yellow: colors.amber,
            green: colors.green,
            blue: colors.lightBlue,
            indigo: colors.indigo,
            purple: colors.purple,
            pink: colors.pink,
        },
    },
    variants: {
        extend: {},
    },
    plugins: [],
}
