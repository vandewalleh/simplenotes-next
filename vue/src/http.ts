export class Http {
    private headers = new Map()

    constructor(private readonly baseUrl: string) {
        this.headers.set('Content-Type', 'application/json')
        this.headers.set('Accept', 'application/json')
    }

    setHeader(header: string, value: string) {
        this.headers.set(header, value)
    }

    removeHeader(header: string) {
        this.headers.delete(header)
    }

    post(path: string, body: any): Promise<Response> {
        return fetch(`${this.baseUrl}${path}`, {
            method: 'POST',
            body: JSON.stringify(body),
            headers: Object.fromEntries(this.headers),
        })
    }

    get(path: string): Promise<Response> {
        return fetch(`${this.baseUrl}${path}`, {
            method: 'GET',
            headers: Object.fromEntries(this.headers),
        })
    }
}
