import { h, defineComponent } from 'vue'

const classes = [
    'w-full rounded block border-blue-200 box-border border-2 p-2',
    'focus:placeholder-blue-600 focus:border-blue-500 focus:outline-none',
]

export default defineComponent({
    props: {
        area: {
            type: Boolean,
            default: false,
        },
        modelValue: {
            type: String,
            default: '',
        },
    },
    emits: ['update:modelValue'],
    render() {
        const tag = this.area ? 'textarea' : 'input'

        if (this.area) classes.push('resize-none')

        return h(tag, {
            class: classes,
            modelValue: this.modelValue,
            'onUpdate:modelValue': (value: any) => this.$emit('update:modelValue', value),
            onInput: ($event: { target: any }) => this.$emit('update:modelValue', $event.target.value),
        })
    },
})
