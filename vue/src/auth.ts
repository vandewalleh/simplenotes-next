import { Api, LoginError, Token } from './api'
import { router } from './router'
import { reactive, computed, watch, ComputedRef } from 'vue'

export const auth: {
    token: string | null
    username: string | null
} = reactive({
    token: null,
    username: null,
})

export const loggedIn: ComputedRef<boolean> = computed(() => !!auth.token)

function parseJwt(token: string) {
    const base64Url = token.split('.')[1]
    const base64 = base64Url.replace(/-/g, '+').replace(/_/g, '/')
    const jsonPayload = decodeURIComponent(
        atob(base64)
            .split('')
            .map((c) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2))
            .join('')
    )

    return JSON.parse(jsonPayload)
}

watch(
    () => auth.token,
    (token) => {
        if (token) {
            auth.username = parseJwt(token)['username']
            Api.setToken(token)
            localStorage.setItem('token', token)
        } else {
            auth.username = null
            Api.clearToken()
            localStorage.removeItem('token')
        }
    }
)

function init() {
    const token = localStorage.getItem('token')
    if (token) {
        auth.token = token
    }
}

init()

export async function login(form: { username: string; password: string }): Promise<LoginError | null> {
    const loginResult = await Api.login(form)
    if (loginResult instanceof Token) {
        auth.token = loginResult.value
        await router.push('/')
        return null
    } else return loginResult
}

export async function logout() {
    auth.token = null
    await router.push('/')
}
