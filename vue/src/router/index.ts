import { createRouter, createWebHistory } from 'vue-router'
import Home from '../pages/Home.vue'
import Login from '../pages/Login.vue'
import Notes from '../pages/notes/Index.vue'
import List from '../pages/notes/List.vue'
import New from '../pages/notes/New.vue'
import Trash from '../pages/notes/Trash.vue'
import Drafts from '../pages/notes/Drafts.vue'

export const router = createRouter({
    history: createWebHistory(),
    routes: [
        {
            path: '/',
            name: 'home',
            component: Home,
        },
        {
            path: '/login',
            name: 'login',
            component: Login,
        },
        {
            path: '/notes',
            name: 'notes',
            component: Notes,
            redirect: '/notes/list',
            children: [
                {
                    path: 'list',
                    component: List,
                },
                {
                    path: 'new',
                    component: New,
                },
                {
                    path: 'drafts',
                    component: Drafts,
                },
                {
                    path: 'trash',
                    component: Trash,
                },
            ],
        },
    ],
})
