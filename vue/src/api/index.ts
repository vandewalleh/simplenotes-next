import { Http } from '../http'

export enum LoginError {
    InvalidCredentials,
    NotFound,
    Other,
}

export class Token {
    value: string

    constructor(value: string) {
        this.value = value
    }
}

export type LoginResponse = Token | LoginError

// @ts-ignore
const http = new Http(import.meta.env.VITE_API_URL)

export namespace Api {
    export function setToken(token: string) {
        http.setHeader('Authorization', `Bearer ${token}`)
    }

    export function clearToken() {
        http.removeHeader('Authorization')
    }

    export async function login(form: { username: string; password: string }): Promise<LoginResponse> {
        try {
            const response = await http.post('/login', form)
            switch (response.status) {
                case 200:
                    const body = await response.json()
                    return new Token(body['token'])
                case 401:
                    return LoginError.InvalidCredentials
                case 404:
                    return LoginError.NotFound
                default:
                    return LoginError.Other
            }
        } catch (e) {
            return LoginError.Other
        }
    }

    export async function createNote(note: { title: string; tags: string[]; content: string }): Promise<Response> {
        return await http.post('/notes', note)
    }

    export async function findAllNotes(): Promise<NoteWithLastModified[]> {
        const response = await http.get('/notes')
        return await response.json()
    }
}
