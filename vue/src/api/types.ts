type NoteWithLastModified = {
    title: string
    content: string
    tags: string[]
    lastModified: string
}
